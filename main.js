async function fetchJson(url) {
    const response = await fetch(url);
    return await response.json();
}

/**
 *
 * @param planets массив планет
 * @param clearPlanetsList флаг очищать ли список планет (на странице)
 */
function renderPlanetsList(planets, clearPlanetsList = true) {
    const list = document.querySelector('#planet_list');
    if (clearPlanetsList) {
        list.innerHTML = "";
    }
    for (let i = 0; i < planets.results.length; i++) {
        const el = createPlanetCardEl(planets.results[i]);
        // console.log(el);
        list.appendChild(el);
    }
}

/**
 * Создание карточки  планеты
 * Пример planet:
 *
 * {
			"name": "Geonosis",
			"rotation_period": "30",
			"orbital_period": "256",
			"diameter": "11370",
			"climate": "temperate, arid",
			"gravity": "0.9 standard",
			"terrain": "rock, desert, mountain, barren",
			"surface_water": "5",
			"population": "100000000000",
			"residents": [
				"https://swapi.dev/api/people/63/"
			],
			"films": [
				"https://swapi.dev/api/films/5/"
			],
			"created": "2014-12-10T12:47:22.350000Z",
			"edited": "2014-12-20T20:58:18.437000Z",
			"url": "https://swapi.dev/api/planets/11/"
		}

 * @param planet объект/json с информацией о планете
 * @returns {HTMLDivElement}
 */
function createPlanetCardEl(planet) {
    // console.log(planet);
    const elCard = document.createElement('div');
    elCard.classList.add('card', 'm-2');
    elCard.setAttribute('style', 'width: 16rem;');

    const elCardBody = document.createElement('div');
    elCardBody.classList.add('card-body');
    const elCardBodyTitle = document.createElement('h5');
    elCardBodyTitle.classList.add('card-title');
    elCardBodyTitle.innerText = planet.name;
    elCardBody.appendChild(elCardBodyTitle);
    elCard.appendChild(elCardBody);

    const elUl = document.createElement('ul');
    elUl.classList.add('list-group', 'list-group-flush');

    const elli = document.createElement('li');
    elli.classList.add('list-group-item');
    elli.innerText = 'Diameter: ' + planet.diameter;
    elUl.appendChild(elli);

    elUl.appendChild(createElLi('Population: ' + planet.population));
    elUl.appendChild(createElLi('Gravity: ' + planet.gravity));
    elUl.appendChild(createElLi('Terrain: ' + planet.terrain));
    elUl.appendChild(createElLi('Climate: ' + planet.climate));

    elCard.appendChild(elUl);

    const elButtonMoreInfo = document.createElement('button');
    elButtonMoreInfo.classList.add('btn', 'btn-primary', 'open-modal', 'm-2');
    elButtonMoreInfo.setAttribute('type', 'button');
    elButtonMoreInfo.setAttribute('data-bs-toggle', 'modal');
    elButtonMoreInfo.setAttribute('data-bs-target', '#exampleModal');
    elButtonMoreInfo.innerText = "More information";
    elCard.appendChild(elButtonMoreInfo);

    elButtonMoreInfo.addEventListener('click', function (event) {
        renderModal(planet.url);
    })

    return elCard;
}

/**
 *
 * @param planets массив планет
 * @param planetsUrl  адрес по которому запросили этот массив планет
 */
function renderPagination(planets, planetsUrl) {
    const elUl = document.querySelector("#pag-ul");
    elUl.innerHTML = "";

    const elLiPrevious = createPaginationLiEl('previous');
    if (planets.previous) {
        elLiPrevious.addEventListener('click',
            function (event) {
                render(planets.previous);
            });
    } else {
        elLiPrevious.classList.add('disabled');
    }
    elUl.appendChild(elLiPrevious);

    // 1 2 3 4
    const countPages = Math.ceil(planets.count / 10);
    for (let i = 0; i < countPages; i++) {
        const pageInfoLink = 'https://swapi.dev/api/planets/?page=' + (i + 1);
        const elLi = createPaginationLiEl(i + 1);
        if (pageInfoLink === planetsUrl) {
            elLi.classList.add('active');
        }
        elLi.addEventListener('click', function (event) {
            render(pageInfoLink);
        });
        elUl.appendChild(elLi);
    }

    const elLiNext = createPaginationLiEl('next');
    if (planets.next) {
        elLiNext.addEventListener('click',
            function (event) {
                render(planets.next);
            });
    } else {
        elLiNext.classList.add('disabled');
    }
    elUl.appendChild(elLiNext);

    const elMore = document.querySelector('#pagination-more');
    elMore.innerHTML = "";
    const elMoreA = createPaginationLiEl('more');
    if (planets.next) {
        elMoreA.addEventListener('click', function (ev) {
            render(planets.next, false);
        });
    } else {
        elMoreA.classList.add('disabled');
    }
    elMore.appendChild(elMoreA);
}

function createPaginationLiEl(text) {
    const elLi = document.createElement('li');
    elLi.classList.add('page-item');
    const elLiA = document.createElement('a');
    elLiA.classList.add('page-link');
    elLiA.setAttribute('href', "#!");
    elLiA.innerText = text;
    elLi.appendChild(elLiA);
    return elLi;
}


function createElLi(liText) {
    const elLi = document.createElement('li');
    elLi.classList.add('list-group-item');
    elLi.innerText = liText;
    return elLi;
}


/**
 * загрузить список планет, чтобы отобразить пагинацию и карточки по планетам
 * @param url
 * @param clearPlanetsList
 */
function render(url, clearPlanetsList = true) {
    fetchJson(url)
        .then(function (planets) {
            // console.log(planets);
            renderPlanetsList(planets, clearPlanetsList)
            renderPagination(planets, url);
        });
}

/**
 * Отобразить карточки всех планет
 */
async function renderAllPagesPlanets(planetsUrl) {
    const elListPlanets = document.querySelector('#planet_list');
    elListPlanets.innerHTML = "";
    do {
        const planets = await fetchJson(planetsUrl);
        console.log(planets);
        renderPlanetsList(planets, false)
        renderPagination(planets, planetsUrl);
        planetsUrl = planets.next;
    } while (planetsUrl != null);


}


/**
 * Заполнить модальное окно
 *
 *
 * @param filmsUrls массив адресов фильмов
 */
function renderModalFilms(filmsUrls) {
    // console.log(filmsUrls);
    const fetchs = [];
    for (let i = 0; i < filmsUrls.length; i++) {
        fetchs.push(fetchJson(filmsUrls[i]));
    }

    Promise.all(fetchs)
        .then(function (filmsInfo) {

            const charactersUrls = [];
            const elListFilm = document.querySelector("#list-films");
            elListFilm.innerHTML = "";

            for (let i = 0; i < filmsInfo.length; i++) {

                const elLi = createElLi("#" + filmsInfo[i].episode_id + ", \"" + filmsInfo[i].title + "\", " + filmsInfo[i].release_date);
                elListFilm.appendChild(elLi);

                for (let j = 0; j < filmsInfo[i].characters.length; j++) {
                    charactersUrls.push(filmsInfo[i].characters[j]);
                }
            }

            // console.log(charactersUrls);
            return charactersUrls.reduce(function (previousValue, value, i, charactersUrl) {
                    if (i === charactersUrl.indexOf(value)) {
                        previousValue.push(value);
                    }
                    return previousValue;
                },
                []);
        })
        .then(function (charactersUrl) {
            const fetchs = [];
            for (let i = 0; i < charactersUrl.length; i++) {
                fetchs.push(fetchJson(charactersUrl[i]));
            }
            return Promise.all(fetchs);
        })
        .then(async function (charactersInfo) {
            const elListCharacter = document.querySelector("#list-persons");
            elListCharacter.innerHTML = "";

            for (let i = 0; i < charactersInfo.length; i++) {
                const planetInfo = await fetchJson(charactersInfo[i].homeworld);
                const elLi = createElLi(charactersInfo[i].name + ", " + charactersInfo[i].gender + ", " + charactersInfo[i].birth_year + ", " + planetInfo.name);
                elListCharacter.appendChild(elLi);
            }
        })
        .catch(function (err) {
            alert(err);
        })

}

/**
 * Вывести (отразить) в модальном окне информацию о планете
 */
function renderModal(planetUrl) {
    /*
    0) Создать модальное окно и отобразить его на странице.
    1)Получить информацию по планете по url планеты;
    2) В цикле пройтись по массиву фильмов и для каждого фильма сделать:
    2.1. Загрузить информацию о фильме;
    2.2. Номер эпизода (episode_id), Название (title), Дата выхода (release_date)
    2.3.Создать элемент списка по фильму
    2.4. Добавить элемент к списку
    3) То же самое, что и в п.2, только для персонажей
    4.Создать элементы для основной информации об планете и добавить их в модальное окно.
    5) Сделать так, чтобы модальное окно можно было бы скрыть.
     */
    fetchJson(planetUrl)
        .then(function (planet) {
            // console.log(planet);
            const elModalTitle = document.querySelector('#exampleModalLabel');
            elModalTitle.innerText = planet.name;
            const elList = document.querySelector('#list-info-planet');
            elList.innerHTML = "";
            elList.appendChild(createElLi('Diameter: ' + planet.diameter));
            elList.appendChild(createElLi('Population: ' + planet.population));
            elList.appendChild(createElLi('Gravity: ' + planet.gravity));
            elList.appendChild(createElLi('Terrain: ' + planet.terrain));
            elList.appendChild(createElLi('Climate: ' + planet.climate));
            renderModalFilms(planet.films);
        });
}

function main() {
    render('https://swapi.dev/api/planets/?page=1');
    const elButtonShowAll = document.querySelector('#show_all');
    elButtonShowAll.addEventListener('click', function () {
        renderAllPagesPlanets('https://swapi.dev/api/planets/?page=1');
    })
}

main();

